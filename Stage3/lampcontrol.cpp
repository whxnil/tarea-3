#include "lampcontrol.h"
#include "ui_lampcontrol.h"

LampControl::LampControl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LampControl),
    cloud(nullptr)
{
    ui->setupUi(this);
    connect(ui->onoff, &QPushButton::clicked, this, &LampControl::changeState);
    connect(ui->intensidad, &QSlider::valueChanged, this, &LampControl::changeIntensidad);

}

LampControl::~LampControl()
{
    delete ui;
}

void LampControl::changeState(){
    if(cloud){
        cloud->changeState(getChannel());
    }
}

void LampControl::changeIntensidad(){
    if(cloud){
        cloud->changeIntensidad(getChannel(), ui->intensidad->value());
    }
}

int LampControl::getChannel(){
    ui->spinBox->value();
}

void LampControl::setCloud(Cloud *c){
    cloud = c;
}
