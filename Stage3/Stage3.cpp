#include "mainwindow.h"


#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Cloud cloud;
    MainWindow w(cloud);
    w.show();
    return a.exec();
}
