#include "cloud.h"

Cloud::Cloud()
{


}

void Cloud::addLamp(Lamp *l){
    lampVector.push_back(l);
}
void Cloud::changeState(int channel){
    for(Lamp* l: lampVector){
        if(l->getChannel() == channel){
            l->changeState();
        }
    }
}
void Cloud::changeIntensidad(int channel, int i){
    for(Lamp* l: lampVector){
        if(l->getChannel() == channel){
            l->changeIntensidad(i);
        }
    }
}


