#ifndef CLOUD_H
#define CLOUD_H

#include<vector>
#include "lamp.h"



class Cloud
{
public:
    Cloud();
    void addLamp(Lamp *lamp);
    void changeState(int channel);
    void changeIntensidad(int channel, int i);

private:
    std::vector<Lamp*> lampVector;
};


#endif // CLOUD_H
