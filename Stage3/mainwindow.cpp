#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(Cloud &cloud, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cloud.addLamp(ui->lamp);
    ui->lamp->setChannel(2);

    cloud.addLamp(ui->lamp2);
    ui->lamp2->setChannel(3);

    ui->lampcontrol->setCloud(&cloud);

}

MainWindow::~MainWindow()
{
    delete ui;
}



