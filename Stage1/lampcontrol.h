#ifndef LAMPCONTROL_H
#define LAMPCONTROL_H

#include <QWidget>
#include "cloud.h"

namespace Ui {
class LampControl;
}

class LampControl : public QWidget
{
    Q_OBJECT

public:
    explicit LampControl(QWidget *parent = nullptr);
    void setCloud(Cloud *c);
    int getChannel();

    ~LampControl();

private slots:
    void changeState();

private:
    Ui::LampControl *ui;
    Cloud *cloud;
    int channel;
};

#endif // LAMPCONTROL_H
