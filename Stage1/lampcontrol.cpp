#include "lampcontrol.h"
#include "ui_lampcontrol.h"

LampControl::LampControl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LampControl),
    cloud(nullptr),
    channel(0)
{
    ui->setupUi(this);
    connect(ui->onoff, &QPushButton::clicked, this, &LampControl::changeState);

}

LampControl::~LampControl()
{
    delete ui;
}

void LampControl::changeState(){
    if(cloud){
        cloud->changeState(getChannel());
    }
}

int LampControl::getChannel(){
     return channel;
}

void LampControl::setCloud(Cloud *c){
    cloud = c;
}
