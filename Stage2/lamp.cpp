#include "lamp.h"
#include "ui_lamp.h"

Lamp::Lamp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Lamp),
    channel(0),
    state(0),
    intensidad(255)
{
    ui->setupUi(this);

}

Lamp::~Lamp()
{
    delete ui;
}

void Lamp::changeState(){
    state = !state;
    update();
}
void Lamp::setChannel(int c){
    channel=c;
}
int Lamp::getChannel(){
    return channel;
}

void Lamp::changeIntensidad(int i){
    intensidad = i;
    update();
}



void Lamp::paintEvent(QPaintEvent *e){
    QPainter painter(this);

    QPen pen2(Qt::black,2);

    QPolygon base;
    base << QPoint(18, 20) << QPoint(18, 50)
             << QPoint(13, 50) << QPoint(10, 53)
             << QPoint(10, 60) << QPoint(30, 60)
             << QPoint(30, 53) << QPoint(27, 50)
             << QPoint(22, 50) << QPoint(22, 20);

    QBrush brush2;
    brush2.setColor(Qt::gray);
    brush2.setStyle(Qt::SolidPattern);


    painter.setPen(pen2);


    QPainterPath phat2;
    phat2.addPolygon(base);

    painter.drawPolygon(base);
    painter.fillPath(phat2, brush2);


/////////////////////////////////////////////////////////////////////
    QPen pen(Qt::black,2);

    QPolygon pantalla;
    pantalla<<QPoint(0,20)<<QPoint(10,0)<<QPoint(30,0)<<QPoint(40,20)<<QPoint(0,20);

    QBrush brush;
    brush.setColor(state ? QColor(intensidad, intensidad, intensidad) : Qt::black);
    brush.setStyle(Qt::SolidPattern);


    painter.setPen(pen);


    QPainterPath phat;
    phat.addPolygon(pantalla);

    painter.drawPolygon(pantalla);
    painter.fillPath(phat, brush);


}
