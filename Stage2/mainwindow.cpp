#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(Cloud &cloud, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cloud.addLamp(ui->lamp);
    ui->lamp->setChannel(0);
    ui->lampcontrol->setCloud(&cloud);
}

MainWindow::~MainWindow()
{
    delete ui;
}



