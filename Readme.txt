//Tarea 3:Simulador Gráfico en C++ de Lámpara Domótica

//Integrantes:
	-Nilsson Acevedo 202030523-4

//Archivos:
	-cloud.cpp, cloud.h, lamp.cpp, lamp.h, lampcontrol.cpp, lampcontrol.h, mainwindow.cpp, mainwindow.h, lamp.ui, lampcontrol.ui, mainwindow.ui.
	-Stage[x].pro, que permite abrir en la interfaz de Qt.
	-Carpeta resources que contienen las imagenes utilizadas.

//Como compilar y ejecutar:
	-Tener instalado QtCreator.
	-Abrir el archivo .pro de cada Stage.
	-Compilar y ejecutar el codigo.

//Bonificación:
	-Se opto por realizar el punto 2.4 de la tarea, en este casi fue llamado como "Stage4".
	
