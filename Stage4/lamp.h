#ifndef LAMP_H
#define LAMP_H

#include <QWidget>
#include <QtCore>
#include <QtGui>
#include <QPainter>
#include <QPolygon>

namespace Ui {
class Lamp;
}

class Lamp : public QWidget
{
    Q_OBJECT

public:
    explicit Lamp(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent *e);
    int getChannel();
    void changeState();
    void changeIntensidad(int r, int g, int b);
    void setChannel(int c);
    ~Lamp();

private:
    Ui::Lamp *ui;
    QPolygon *base, *pantalla;
    int channel;
    int state;
    int r, g, b;
};

#endif // LAMP_H
