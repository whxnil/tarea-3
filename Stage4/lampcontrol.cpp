#include "lampcontrol.h"
#include "ui_lampcontrol.h"

LampControl::LampControl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LampControl),
    cloud(nullptr)
{
    ui->setupUi(this);
    ui->rgbLayout_2->setVisible(false);
    connect(ui->onoff, &QPushButton::clicked, this, &LampControl::changeState);
    connect(ui->intensidad, &QSlider::valueChanged, this, &LampControl::changeIntensidad);
    connect(ui->red,&QSlider::valueChanged, this, &LampControl::changeIntensidad);
    connect(ui->green,&QSlider::valueChanged, this, &LampControl::changeIntensidad);
    connect(ui->blue,&QSlider::valueChanged, this, &LampControl::changeIntensidad);

}

LampControl::~LampControl()
{
    delete ui;
}

void LampControl::changeState(){
    if(cloud){
        cloud->changeState(getChannel());
    }
}

void LampControl::changeIntensidad(){
    if(cloud){
        if(ui->horizontalWidget->isVisible()){
           int i = ui->intensidad->value();
           cloud->changeIntensidad(getChannel(), i,i,i );
        }
        else{
            cloud->changeIntensidad(getChannel(), ui->red->value(), ui->green->value(), ui->blue->value());
        }

    }
}

void LampControl::changeControl(){
    if(ui->horizontalWidget->isVisible()){
        ui->horizontalWidget->setVisible(false);
        ui->rgbLayout_2->setVisible(true);
    }
    else{
        ui->horizontalWidget->setVisible(true);
        ui->rgbLayout_2->setVisible(false);
    }
}

int LampControl::getChannel(){
    ui->spinBox->value();
}

void LampControl::setCloud(Cloud *c){
    cloud = c;
}
