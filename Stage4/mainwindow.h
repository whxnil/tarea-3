#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QtCore>
#include <QtGui>
#include "cloud.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Cloud &cloud, QWidget *parent = nullptr);

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QAction *changeControl;
    QMenu *options;
};
#endif // MAINWINDOW_H
