#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(Cloud &cloud, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    changeControl= new QAction(tr("&ControlRGB/ControlIntensidad"), this);
    connect(changeControl, &QAction::triggered, ui->lampcontrol, &LampControl::changeControl);
    options = ui->menu->addMenu(tr("&Controles"));
    options->addAction(changeControl);

    cloud.addLamp(ui->lamp);
    ui->lamp->setChannel(2);

    cloud.addLamp(ui->lamp2);
    ui->lamp2->setChannel(3);

    ui->lampcontrol->setCloud(&cloud);

}

MainWindow::~MainWindow()
{
    delete ui;
}



